# Back-End

### Go + Fiber + Mysql
##
##
#### Help links
 - https://go.dev/
 - https://pkg.go.dev/github.com/gofiber/fiber@v1.14.6
 - https://github.com/gofiber/fiber
 - https://docs.gofiber.io/
 - https://pkg.go.dev/github.com/go-sql-driver/mysql
 - https://tutorialedge.net/golang/golang-mysql-tutorial/
 - https://www.golangprograms.com/example-of-golang-crud-using-mysql-from-scratch.html
 - https://zetcode.com/golang/mysql/
 
#### Observações
Lembrando que para rodar esse projeto em sua maquina você precisa ter instalado go e o servidor mysql.
a estrutura de banco utilizada é bem simples.
No arquivo `.env` do projeto substitua as informações da sua conexão com o banco. 
#
#### Crie a tabela people e product
#
   ```SQL
       CREATE TABLE `people` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(100) NOT NULL,
      `email` varchar(100) NOT NULL,
      `age` int(11) NOT NULL,
      `state` smallint(6) DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1
   ```
   
   ```SQL
    CREATE TABLE `product` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `description` varchar(100) NOT NULL,
      `sales_vl` double(12,2) DEFAULT '0.00',
      `state` smallint(6) DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1
   ```
