package main

import (
	"os"

	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
	"gitlab.com/golang-studies/api-go-m1/back-end/cmd/server/routes"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/repositories/config"
)

func main() {
	godotenv.Load()

	r := routes.NewRoutes()

	r.App.Use(cors.New())

	i := config.GetConfigRepository()
	defer i.DB.Close()

	r.RegisterRoutesGet("src")
	r.RegisterRoutesPost("adm")

	p := os.Getenv("PORT")
	r.App.Listen(":" + p)
}

// https://github.com/go-sql-driver/mysql
// https://tutorialedge.net/golang/golang-mysql-tutorial/
// https://zetcode.com/golang/mysql/

// https://github.com/gofiber/fiber
// https://docs.gofiber.io/
