package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/request"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/services"
)

type Handlers struct {
	SvrPessoas  *services.PessoasService
	SvrProdutos *services.ProdutosService
}

func NewHandlers() *Handlers {
	return &Handlers{}
}

func (h *Handlers) GetPessoas(c *fiber.Ctx) error {
	ctx := c.Context()
	source := c.Query("source")
	var rqPessoas *request.PessoasGetRq
	if len(source) > 0 {
		err := json.Unmarshal([]byte(source), &rqPessoas)
		if err != nil {
			return err
		}
	}

	rsp, er := h.SvrPessoas.GetPessoas(ctx, rqPessoas)
	if er != nil {
		return c.Status(er.Status).JSON(er)
	}
	return c.Status(http.StatusOK).JSON(rsp)
}

func (h *Handlers) GetProdutos(c *fiber.Ctx) error {
	ctx := c.Context()
	source := c.Query("source")
	var rqProdutos *request.ProdutosGetRq
	if len(source) > 0 {
		err := json.Unmarshal([]byte(source), &rqProdutos)
		if err != nil {
			return err
		}
	}

	rsp, er := h.SvrProdutos.GetProdutos(ctx, rqProdutos)
	if er != nil {
		return c.Status(er.Status).JSON(er)
	}
	return c.Status(http.StatusOK).JSON(rsp)
}

func (h *Handlers) PostPessoas(c *fiber.Ctx) error {
	ctx := c.Context()
	data := c.Request().Body()

	var rqPessoas *request.PessoasPostRq
	err := json.Unmarshal(data, &rqPessoas)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(err.Error())
	}

	rsp, er := h.SvrPessoas.PostPessoas(ctx, rqPessoas)
	if er != nil {
		return c.Status(er.Status).JSON(er)
	}
	return c.Status(http.StatusOK).JSON(rsp)
}

func (h *Handlers) PatchPessoas(c *fiber.Ctx) error {
	ctx := c.Context()
	data := c.Request().Body()
	fmt.Println(string(data))

	var rqPessoas *request.PessoasPostRq
	err := json.Unmarshal(data, &rqPessoas)
	if err != nil {
		return err
	}

	rsp, er := h.SvrPessoas.PatchPessoas(ctx, rqPessoas)
	if er != nil {
		return c.Status(er.Status).JSON(er)
	}
	return c.Status(http.StatusOK).JSON(rsp)
}

func (h *Handlers) PostProdutos(c *fiber.Ctx) error {
	ctx := c.Context()
	data := c.Request().Body()

	var rqProdutos *request.ProdutosPostRq
	err := json.Unmarshal(data, &rqProdutos)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(err.Error())
	}

	rsp, er := h.SvrProdutos.PostProdutos(ctx, rqProdutos)
	if er != nil {
		return c.Status(er.Status).JSON(er)
	}
	return c.Status(http.StatusOK).JSON(rsp)
}

func (h *Handlers) PatchProdutos(c *fiber.Ctx) error {
	ctx := c.Context()
	data := c.Request().Body()

	var rqProdutos *request.ProdutosPostRq
	err := json.Unmarshal(data, &rqProdutos)
	if err != nil {
		return err
	}

	rsp, er := h.SvrProdutos.PatchProdutos(ctx, rqProdutos)
	if er != nil {
		return c.Status(er.Status).JSON(er)
	}
	return c.Status(http.StatusOK).JSON(rsp)
}
