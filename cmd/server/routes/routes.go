package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-studies/api-go-m1/back-end/cmd/server/routes/handlers"
)

type Routes struct {
	App     *fiber.App
	Handler *handlers.Handlers
}

func NewRoutes() *Routes {
	return &Routes{
		App:     fiber.New(),
		Handler: handlers.NewHandlers(),
	}
}

func (r *Routes) RegisterRoutesGet(group string) {
	api := r.App.Group(group)
	api.Get("pessoas", r.Handler.GetPessoas)
	api.Get("produtos", r.Handler.GetProdutos)
}

func (r *Routes) RegisterRoutesPost(group string) {
	api := r.App.Group(group)

	api.Post("pessoas", r.Handler.PostPessoas)
	api.Patch("pessoas", r.Handler.PatchPessoas)
	api.Post("produtos", r.Handler.PostProdutos)
	api.Patch("produtos", r.Handler.PatchProdutos)
}
