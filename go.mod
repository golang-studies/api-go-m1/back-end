module gitlab.com/golang-studies/api-go-m1/back-end

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/jinzhu/copier v0.3.2
	github.com/joho/godotenv v1.3.0
)
