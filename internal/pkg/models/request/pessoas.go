package request

type PessoasPostRq struct {
	Id     string `json: "id"`
	Nome   string `json: "nome"`
	Email  string `json: "email"`
	Idade  string `json: "idade"`
	Status bool   `json: "status"`
}

type PessoasGetRq struct {
	Nome string `json: "nome"`
}
