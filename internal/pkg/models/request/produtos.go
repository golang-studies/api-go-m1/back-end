package request

type ProdutosPostRq struct {
	Id        string `json: "id"`
	Descricao string `json: "descricao"`
	Valor     string `json: "valor"`
	Status    bool   `json: "status"`
}

type ProdutosGetRq struct {
	Descricao string `json: "descricao"`
}
