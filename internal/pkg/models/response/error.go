package response

type ErrorResponse struct {
	Status   int    `json "status"`
	Mensagem string `json "mensagem"`
}

func NewErrorResponse(status int, mensagem string) *ErrorResponse {
	return &ErrorResponse{
		Status:   status,
		Mensagem: mensagem,
	}
}
