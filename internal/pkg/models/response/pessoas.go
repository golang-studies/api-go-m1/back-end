package response

type PessoaResponse struct {
	Id     int    `json "id"`
	Nome   string `json "nome"`
	Email  string `json: "email"`
	Idade  int    `json: "idade"`
	Status int    `json: "status"`
}

type PessoasResponse struct {
	Pessoas []*PessoaResponse `json "pessoas"`
}
