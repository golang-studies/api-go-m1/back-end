package response

type ProdutoResponse struct {
	Id        int     `json "id"`
	Descricao string  `json "descricao"`
	Valor     float64 `json: "valor"`
	Status    int     `json: "status"`
}

type ProdutosResponse struct {
	Produtos []*ProdutoResponse `json "produtos"`
}
