package pessoas

import (
	"strconv"

	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/request"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/response"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/repositories/config"
)

type PessoasRepository struct {
	instance *config.ConfigRepository
}

func NewPessoasRepository() *PessoasRepository {
	return &PessoasRepository{
		instance: config.GetConfigRepository(),
	}
}

func (r *PessoasRepository) Get(filtro string) (*response.PessoasResponse, error) {

	sql := "SELECT * FROM people "
	if len(filtro) > 0 {
		sql += "where name like '%" + filtro + "%' "
	}
	results, err := r.instance.DB.Query(sql)
	if err != nil {
		return nil, err
	}

	rspPessoas := new(response.PessoasResponse)
	for results.Next() {
		pessoa := new(response.PessoaResponse)

		err = results.Scan(&pessoa.Id, &pessoa.Nome, &pessoa.Email, &pessoa.Idade, &pessoa.Status)
		if err != nil {
			continue
		}
		rspPessoas.Pessoas = append(rspPessoas.Pessoas, pessoa)
	}
	results.Close()
	return rspPessoas, nil
}

func (r *PessoasRepository) Insert(rqPessoas *request.PessoasPostRq) (*response.PessoaResponse, error) {
	status := "0"
	if !rqPessoas.Status {
		status = "1"
	}

	sql := "INSERT INTO people(id, name, email, age, state) "
	sql += "VALUES (default, '" + rqPessoas.Nome + "', '" + rqPessoas.Email + "', '" + rqPessoas.Idade + "', '" + status + "'); "

	res, err := r.instance.DB.Exec(sql)
	if err != nil {
		return nil, err
	}

	lastId, err2 := res.LastInsertId()
	if err2 != nil {
		return nil, err
	}

	rsp := new(response.PessoaResponse)
	rsp.Id = int(lastId)
	rsp.Nome = rqPessoas.Nome
	rsp.Email = rqPessoas.Email
	rsp.Idade, _ = strconv.Atoi(rqPessoas.Idade)
	rsp.Status, _ = strconv.Atoi(status)
	return rsp, nil
}

func (r *PessoasRepository) Update(rqPessoas *request.PessoasPostRq) (*response.PessoaResponse, error) {
	status := "0"
	if !rqPessoas.Status {
		status = "1"
	}

	sql := "UPDATE people SET name ='" + rqPessoas.Nome + "',email ='" + rqPessoas.Email + "',age = '" + rqPessoas.Idade + "',state = '" + status + "'"
	sql += "WHERE id = " + rqPessoas.Id

	_, err := r.instance.DB.Exec(sql)
	if err != nil {
		return nil, err
	}

	rsp := new(response.PessoaResponse)
	rsp.Id, _ = strconv.Atoi(rqPessoas.Id)
	rsp.Nome = rqPessoas.Nome
	rsp.Email = rqPessoas.Email
	rsp.Idade, _ = strconv.Atoi(rqPessoas.Idade)
	rsp.Status, _ = strconv.Atoi(status)
	return rsp, nil
}
