package produtos

import (
	"fmt"
	"strconv"

	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/request"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/response"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/repositories/config"
)

type ProdutosRepository struct {
	instance *config.ConfigRepository
}

func NewProdutosRepository() *ProdutosRepository {
	return &ProdutosRepository{
		instance: config.GetConfigRepository(),
	}
}

func (r *ProdutosRepository) Get(filtro string) (*response.ProdutosResponse, error) {

	sql := "SELECT * FROM product "
	if len(filtro) > 0 {
		sql += "where description like '%" + filtro + "%' "
	}
	results, err := r.instance.DB.Query(sql)
	if err != nil {
		return nil, err
	}

	rspProdutos := new(response.ProdutosResponse)
	for results.Next() {
		produto := new(response.ProdutoResponse)

		err = results.Scan(&produto.Id, &produto.Descricao, &produto.Valor, &produto.Status)
		if err != nil {
			continue
		}
		rspProdutos.Produtos = append(rspProdutos.Produtos, produto)
	}
	results.Close()
	return rspProdutos, nil
}

func (r *ProdutosRepository) Insert(rqProdutos *request.ProdutosPostRq) (*response.ProdutoResponse, error) {
	status := "0"
	if !rqProdutos.Status {
		status = "1"
	}
	sql := "INSERT INTO product(id, description, sales_vl, state) "
	sql += "VALUES (default, '" + rqProdutos.Descricao + "', '" + rqProdutos.Valor + "', '" + status + "'); "

	res, err := r.instance.DB.Exec(sql)
	if err != nil {
		return nil, err
	}

	lastId, err2 := res.LastInsertId()
	if err2 != nil {
		return nil, err2
	}

	rsp := new(response.ProdutoResponse)
	rsp.Id = int(lastId)
	rsp.Descricao = rqProdutos.Descricao
	rsp.Valor, _ = strconv.ParseFloat(rqProdutos.Valor, 32)
	rsp.Status, _ = strconv.Atoi(status)
	return rsp, nil
}

func (r *ProdutosRepository) Update(rqProdutos *request.ProdutosPostRq) (*response.ProdutoResponse, error) {
	status := "0"
	if !rqProdutos.Status {
		status = "1"
	}

	sql := "UPDATE product SET description ='" + rqProdutos.Descricao + "',sales_vl ='" + rqProdutos.Valor + "',state = '" + status + "'"
	sql += "WHERE id = " + rqProdutos.Id
	fmt.Println(sql)
	_, err := r.instance.DB.Exec(sql)
	if err != nil {
		return nil, err
	}

	rsp := new(response.ProdutoResponse)
	rsp.Id, _ = strconv.Atoi(rqProdutos.Id)
	rsp.Descricao = rqProdutos.Descricao
	rsp.Valor, _ = strconv.ParseFloat(rqProdutos.Valor, 32)
	rsp.Status, _ = strconv.Atoi(status)
	return rsp, nil
}
