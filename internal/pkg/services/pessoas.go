package services

import (
	"context"
	"net/http"

	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/request"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/response"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/repositories/pessoas"
)

type PessoasService struct {
}

func NewPessoasService() *PessoasService {
	return &PessoasService{}
}

func (s *PessoasService) GetPessoas(ctx context.Context, rqPessoas *request.PessoasGetRq) (*response.PessoasResponse, *response.ErrorResponse) {
	nome := ""
	if rqPessoas != nil {
		nome = rqPessoas.Nome
	}
	r := pessoas.NewPessoasRepository()

	rsp, err := r.Get(nome)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}

func (s *PessoasService) PostPessoas(ctx context.Context, rqPessoas *request.PessoasPostRq) (*response.PessoaResponse, *response.ErrorResponse) {
	if len(rqPessoas.Nome) < 1 {
		return nil, response.NewErrorResponse(1, "Informe o nome!")
	}
	if len(rqPessoas.Email) < 1 {
		return nil, response.NewErrorResponse(1, "Informe o email!")
	}

	r := pessoas.NewPessoasRepository()
	rsp, err := r.Insert(rqPessoas)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}

	return rsp, nil
}

func (s *PessoasService) PatchPessoas(ctx context.Context, rqPessoas *request.PessoasPostRq) (*response.PessoaResponse, *response.ErrorResponse) {
	if len(rqPessoas.Nome) < 1 {
		return nil, response.NewErrorResponse(1, "Informe o nome!")
	}
	if len(rqPessoas.Email) < 1 {
		return nil, response.NewErrorResponse(1, "Informe o email!")
	}

	r := pessoas.NewPessoasRepository()
	rsp, err := r.Update(rqPessoas)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}
