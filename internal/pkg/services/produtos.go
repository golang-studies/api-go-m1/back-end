package services

import (
	"context"
	"net/http"

	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/request"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/models/response"
	"gitlab.com/golang-studies/api-go-m1/back-end/internal/pkg/repositories/produtos"
)

type ProdutosService struct {
}

func NewProdutosService() *ProdutosService {
	return &ProdutosService{}
}

func (s *ProdutosService) GetProdutos(ctx context.Context, rqProdutos *request.ProdutosGetRq) (*response.ProdutosResponse, *response.ErrorResponse) {
	descricao := ""
	if rqProdutos != nil {
		descricao = rqProdutos.Descricao
	}
	r := produtos.NewProdutosRepository()
	rsp, err := r.Get(descricao)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}

func (s *ProdutosService) PostProdutos(ctx context.Context, rqProdutos *request.ProdutosPostRq) (*response.ProdutoResponse, *response.ErrorResponse) {
	if len(rqProdutos.Descricao) < 1 {
		return nil, response.NewErrorResponse(1, "Informe a descriçao!")
	}

	r := produtos.NewProdutosRepository()
	rsp, err := r.Insert(rqProdutos)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}

func (s *ProdutosService) PatchProdutos(ctx context.Context, rqProdutos *request.ProdutosPostRq) (*response.ProdutoResponse, *response.ErrorResponse) {
	if len(rqProdutos.Descricao) < 1 {
		return nil, response.NewErrorResponse(1, "Informe a descriçao!")
	}

	r := produtos.NewProdutosRepository()
	rsp, err := r.Update(rqProdutos)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}
